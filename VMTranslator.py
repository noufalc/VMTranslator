import sys
import os
class Parser:
    def __init__(self, inputFileName):
        if inputFileName[-2:] != 'vm':
            print 'Wrong input file format'
            exit()
        self.inputFile = open(inputFileName)
        self.inputFileName = inputFileName
        self.tempFile = open(inputFileName+'temp', 'w')
        for line in self.inputFile:
            if not line.startswith('//'):
                if not line in ['\n', '\r\n']:
                    self.tempFile.write(line)
        self.inputFile.close()
        self.tempFile.close()
        self.tempFile = open(inputFileName+'temp', 'r')
        self.currentLine = ''
        self.currentCommandType = ''
        self.currentArg1 = ''
        self.words = []

    def close(self):
        self.tempFile.close()
        os.remove(self.inputFileName+'temp')

    def hasMoreCommands(self):
        try:
            self.currentLine = self.tempFile.next()
            return True
        except StopIteration:
            return False

    def advance(self):
       #print self.currentLine,
       pass

    def commandType(self):
        self.currentLine = self.currentLine.strip()
        self.words = self.currentLine.split()
        if self.words != [] and self.words[0] != '//' :
            firstWord = self.words[0]
            print self.currentLine
            if firstWord == 'add' or  firstWord == 'sub' or firstWord == 'neg' or \
                    firstWord == 'eq' or firstWord == 'gt' or firstWord == 'lt' or \
                    firstWord == 'and' or firstWord == 'or' or firstWord == 'not':
                self.currentCommandType = 'C_ARITHMETIC'
            elif firstWord == 'push':
                self.currentCommandType = 'C_PUSH'
            elif firstWord == 'pop':
                self.currentCommandType = 'C_POP'
            elif firstWord == 'label':
                self.currentCommandType = 'C_LABEL'
            elif firstWord == 'goto':
                self.currentCommandType = 'C_GOTO'
            elif firstWord == 'if':
                self.currentCommandType = 'C_IF'
            elif firstWord == 'function':
                self.currentCommandType = 'C_FUNCTION'
            elif firstWord == 'return':
                self.currentCommandType = 'C_RETURN'
            elif firstWord == 'call':
                self.currentCommandType = 'C_CALL'
            else:
                self.currentCommandType = 'BAD_COMMAND'
        self.currentArg1 = firstWord
        return self.currentCommandType 

    def arg1(self):
        return self.currentArg1

    def arg2(self):
        return self.words[1]

    def arg3(self):
        return self.words[2]

class CodeWriter:
    def __init__(self, outputFileName):
        self.outputFile = open(outputFileName[:-2]+'asm', 'w')
        self.staticLabel = outputFileName.split('/')[-1][:-2]
        self.eqLabelCount = 0
        self.gtLabelCount = 0
        self.ltLabelCount = 0

    def writeArithmetic(self, command):
        self.outputFile.write('// ' + str(command))
        if command ==  'add':
            outString = '\n@SP\nA=M\nA=A-1\nD=M\nA=A-1\nM=M+D\nA=A+1\nD=A\n@SP\nM=D\n'
        elif command ==  'sub':
            outString = '\n@SP\nA=M\nA=A-1\nD=M\nA=A-1\nM=M-D\nA=A+1\nD=A\n@SP\nM=D\n'
        elif command ==  'neg':
            outString = '\n@SP\nA=M\nA=A-1\nM=-M\nA=A+1\nD=A\n@SP\nM=D\n'
        elif command ==  'eq':
            self.eqLabelCount += 1
            outCommands = [
                    '@SP',
                    'AM=M-1',
                    'D=M',
                    'A=A-1',
                    'D=M-D',
                    '@SP',
                    'M=M-1',
                    '@_EQUAL_'+str(self.eqLabelCount),
                    'D;JEQ',
                    '@SP',
                    'A=M',
                    'M=0',
                    '@_EQUAL_'+str(self.eqLabelCount)+'_END',
                    '0;JEQ',
                    '(_EQUAL_'+str(self.eqLabelCount)+')',
                    '@SP',
                    'A=M',
                    'M=-1',
                    '(_EQUAL_'+str(self.eqLabelCount)+'_END)',
                    '@SP',
                    'M=M+1']
            outString = '\n'.join(outCommands)
            outString = '\n'+outString+'\n'

        elif command ==  'gt':
            self.gtLabelCount += 1
            outCommands = [
                    '@SP',
                    'AM=M-1',
                    'D=M',
                    'A=A-1',
                    'D=M-D',
                    '@SP',
                    'M=M-1',
                    '@_GT_'+str(self.gtLabelCount),
                    'D;JGT',
                    '@SP',
                    'A=M',
                    'M=0',
                    '@_GT_'+str(self.gtLabelCount)+'_END',
                    '0;JEQ',
                    '(_GT_'+str(self.gtLabelCount)+')',
                    '@SP',
                    'A=M',
                    'M=-1',
                    '(_GT_'+str(self.gtLabelCount)+'_END)',
                    '@SP',
                    'M=M+1']
            outString = '\n'.join(outCommands)
            outString = '\n'+outString+'\n'

        elif command ==  'lt':
            self.ltLabelCount += 1
            outCommands = [
                    '@SP',
                    'AM=M-1',
                    'D=M',
                    'A=A-1',
                    'D=M-D',
                    '@SP',
                    'M=M-1',
                    '@_LT_'+str(self.ltLabelCount),
                    'D;JLT',
                    '@SP',
                    'A=M',
                    'M=0',
                    '@_LT_'+str(self.ltLabelCount)+'_END',
                    '0;JEQ',
                    '(_LT_'+str(self.ltLabelCount)+')',
                    '@SP',
                    'A=M',
                    'M=-1',
                    '(_LT_'+str(self.ltLabelCount)+'_END)',
                    '@SP',
                    'M=M+1']
            outString = '\n'.join(outCommands)
            outString = '\n'+outString+'\n'

        elif command ==  'and':
            outString = '\n@SP\nA=M\nA=A-1\nD=M\nA=A-1\nM=D&M\nA=A+1\nD=A\n@SP\nM=D\n'

        elif command ==  'or':
            outString = '\n@SP\nA=M\nA=A-1\nD=M\nA=A-1\nM=D|M\nA=A+1\nD=A\n@SP\nM=D\n'
    
        elif command ==  'not':
            outString = '\n@SP\nA=M\nA=A-1\nM=!M\nA=A+1\nD=A\n@SP\nM=D\n'
        else:
            print 'Undefined arithmetic operation ' + command
            exit()
        self.outputFile.write(outString)

    def writePushPop(self, command, segment, index):
        self.outputFile.write('// ' + str(command) + ' ' + str(segment) + ' ' + str(index))
        if command == 'push':
            if segment == 'constant':
                pushString = '\n@' + str(index) + '\nD=A\n@SP\nA=M\nM=D\n@SP\nM=M+1\n'

            elif segment == 'local':
                pushString = '\n@' + str(index) + '\nD=A\n@LCL\nA=D+M\nD=M\n@SP\nA=M\nM=D\n@SP\nM=M+1\n'

            elif segment == 'argument':
                pushString = '\n@' + str(index) + '\nD=A\n@ARG\nA=D+M\nD=M\n@SP\nA=M\nM=D\n@SP\nM=M+1\n'

            elif segment == 'this':
                pushString = '\n@' + str(index) + '\nD=A\n@THIS\nA=D+M\nD=M\n@SP\nA=M\nM=D\n@SP\nM=M+1\n'

            elif segment == 'that':
                pushString = '\n@' + str(index) + '\nD=A\n@THAT\nA=D+M\nD=M\n@SP\nA=M\nM=D\n@SP\nM=M+1\n'

            elif segment == 'static':
                pushString = '\n@'+self.staticLabel+str(index)+'\nD=M\n@SP\nA=M\nM=D\n@SP\nM=M+1\n'

            elif segment == 'temp':
                if int(index) > 0 and int(index) < 7:
                    pushString = '\n@' + str(index) + '\nD=A\n@5\nA=D+A\nD=M\n@SP\nA=M\nM=D\n@SP\nM=M+1\n'

            elif segment == 'pointer':
                if index != 0 and index != 1:
                    print 'Invalid index for the segment pointer'
                    exit()
                elif index == 0:
                    word = '@THIS'
                else:
                    word = '@THAT'
                outCommands = [
                        word,
                        'D=M',
                        '@SP',
                        'A=M',
                        'M=D',
                        '@SP',
                        'M=M+1']
                outString = '\n'.join(outCommands)
                pushString = '\n'+outString+'\n'
            else:
                print 'Invalid commad: '+ str(command) + ' ' + str(segment) + ' ' + str(index)
                exit()
            self.outputFile.write(pushString)



        if command == 'pop':
            if segment == 'local':
                pushString = '\n@' + str(index) + '\nD=A\n@LCL\nD=D+M\n@R13\nM=D\n@SP\nAM=M-1\nD=M\n@R13\nA=M\nM=D\n'

            elif segment == 'argument':
                pushString = '\n@' + str(index) + '\nD=A\n@ARG\nD=D+M\n@R13\nM=D\n@SP\nAM=M-1\nD=M\n@R13\nA=M\nM=D\n'

            elif segment == 'this':
                pushString = '\n@' + str(index) + '\nD=A\n@THIS\nD=D+M\n@R13\nM=D\n@SP\nAM=M-1\nD=M\n@R13\nA=M\nM=D\n'

            elif segment == 'that':
                pushString = '\n@' + str(index) + '\nD=A\n@THAT\nD=D+M\n@R13\nM=D\n@SP\nAM=M-1\nD=M\n@R13\nA=M\nM=D\n'

            elif segment == 'static':
                pushString = '\n@SP\nAM=M-1\nD=M\n@'+self.staticLabel+str(index)+'\nM=D\n'

            elif segment == 'temp':
                if int(index) > 0 and int(index) < 7:
                    pushString = '\n@' + str(index) + '\nD=A\n@5\nD=D+A\n@R13\nM=D\n@SP\nAM=M-1\nD=M\n@R13\nA=M\nM=D\n'

            elif segment == 'pointer':
                if index != 0 and index != 1:
                    print 'Invalid index for the segment pointer'
                    exit()
                elif index == 0:
                    word = '@THIS'
                else:
                    word = '@THAT'
                outCommands = [
                        '@SP',
                        'AM=M-1',
                        'D=M',
                        word,
                        'M=D']
                pushString = '\n'.join(outCommands)
                pushString = '\n'+pushString+'\n'
            else:
                print 'Invalid commad: '+ str(command) + ' ' + str(segment) + ' ' + str(index)
                exit()
            self.outputFile.write(pushString)

    def close(self):
        self.outputFile.close()


myParser = Parser(sys.argv[1])
myCodeWriter = CodeWriter(sys.argv[1])

while myParser.hasMoreCommands():    
        myParser.advance()
        commandType = myParser.commandType()
        if commandType == 'C_PUSH' or commandType == 'C_POP':
            myCodeWriter.writePushPop(myParser.arg1(), myParser.arg2(), int(myParser.arg3()))
        if commandType == 'C_ARITHMETIC':
            myCodeWriter.writeArithmetic(myParser.arg1())
myParser.close()
myCodeWriter.close()
