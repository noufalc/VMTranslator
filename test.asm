// push static 7
@test.7
D=M
@SP
A=M
M=D
@SP
M=M+1
// pop static 9
@SP
AM=M-1
D=M
@test.9
M=D
// push pointer 0
@THIS
D=M
@SP
A=M
M=D
@SP
M=M+1
// pop pointer 0
@SP
AM=M-1
D=M
@THIS
M=D
// pop temp 1
@1
D=A
@5
D=D+A
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D
// push temp 1
@1
D=A
@5
A=D+A
D=M
@SP
A=M
M=D
@SP
M=M+1
